#include <iostream>
#include <fstream>
#include <stdio.h>
#define up 1
#define down 2
#define right 3
#define left 4

#define ROW 3
#define COL 4

using namespace std;

int m[ROW][COL];
void DeleteNeighbor(int row, int col,int dir)
{
    int v=m[row][col];
    m[row][col]=0;
    if(dir!=up && row-1>=0 && m[row-1][col]==v)
        DeleteNeighbor(row-1,col,up);
    if(dir!=down && row+1<=ROW && m[row+1][col]==v)
        DeleteNeighbor(row+1,col,down);
    if(dir!=left && col-1>=0 && m[row][col-1]==v)
        DeleteNeighbor(row,col-1,left);
    if(dir!=right && col+1<=COL && m[row][col+1]==v)
        DeleteNeighbor(row,col+1,right);
}
void DeleteBlocks(int row, int col)
{
    int v=m[row][col];
    m[row][col]=0;
    if(row-1>=0 && m[row-1][col]==v)
        DeleteNeighbor(row-1,col,up);
    if(row+1<=ROW && m[row+1][col]==v)
        DeleteNeighbor(row+1,col,down);
    if(col-1>=0 && m[row][col-1]==v)
        DeleteNeighbor(row,col-1,left);
    if(col+1<=COL && m[row][col+1]==v)
        DeleteNeighbor(row,col+1,right);
}
void join_down()
{
    for(int c=0;c<COL;c++)
    {
        for(int f=2;f>0;f--)
        {
            if(m[f][c]==0)
            {
                int j=1;
                while(m[f-j][c]==0 && f-j>=0)
                    j++;
                m[f][c]=m[f-j][c];
                m[f-j][c]=0;
            }
        }
    }
}
void join_left()
{
    for(int f=0;f<ROW;f++)
    {
        for(int c=0;c<COL;c++)
        {
            if(m[f][c]==0)
            {
                int j=1;
                if(c+j<COL)
                {
                    m[f][c]=m[f][c+j];
                    m[f][c+j]=0;
                }
            }
        }
    }
}
bool check_matriz()
{
    int c=0;
    for(int i=0;i<ROW;i++)
    {
        for(int j=0;j<COL;j++)
            if(m[i][j]==0)
                c++;
    }
    if(c==12)
        return true;
}
int main()
{
    FILE *archivo=fopen("archivo.txt","r");
    while(feof(archivo)==0)
    {
        for(int i=0;i<ROW;i++)
            for(int j=0;j<COL;j++)
                fscanf(archivo,"%d",&m[i][j]);
    }
    fclose(archivo);
    for(int i=0;i<ROW;i++)
    {
        for(int j=0;j<COL;j++)
            cout<<m[i][j];
        cout<<endl;

    }
    int c=0;
    bool check=check_matriz();
    while(!check)
    {
        int x,y,tmp;
        cout<<"ingrese fila: ";
        cin>>y;
        cout<<"ingrese columna: ";
        cin>>x;
        DeleteBlocks(y,x);
        join_down();
        join_left();
        for(int i=0;i<ROW;i++)
        {
            for(int j=0;j<COL;j++)
                cout<<m[i][j];
            cout<<endl;
        }
        c++;
        check=check_matriz();
    }
    return 0;
}
